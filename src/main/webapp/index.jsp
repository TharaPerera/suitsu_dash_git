<html>
<head>
<title>SuitU</title>
<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
<!-- bootstrap -->
<link href="css/bootstrap.min.css" rel='stylesheet' type='text/css' media="all" />
<!-- //bootstrap -->
<link href="css/dashboard.css" rel="stylesheet">
<!-- Custom Theme files -->
<link href="css/style.css" rel='stylesheet' type='text/css' media="all" />
<script src="js/jquery-1.11.1.min.js"></script>
<!--start-smoth-scrolling-->
<!-- fonts -->
<link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Poiret+One' rel='stylesheet' type='text/css'>

</head>
<body>
  <script>
  window.fbAsyncInit = function() {
    FB.init({
      appId      : '1532830613706813',
      xfbml      : true,
      version    : 'v2.5'
    });

    // ADD ADDITIONAL FACEBOOK CODE HERE
    function onLogin(response) {
      if (response.status == 'connected') {
        FB.api('/me?fields=first_name', function(data) {
          var welcomeBlock = document.getElementById('fb-welcome');
          welcomeBlock.innerHTML = 'Hello, ' + data.first_name + '!';
        });
      }
    }

    FB.getLoginStatus(function(response) {
  // Check login status on load, and if the user is
  // already logged in, go directly to the welcome message.
  if (response.status == 'connected') {
    onLogin(response);
  } else {
    // Otherwise, show Login dialog first.
    FB.login(function(response) {
      onLogin(response);
    }, {scope: 'user_friends, email'});
  }
});
    
    
    
    // CODE END
  };

  (function(d, s, id){
   var js, fjs = d.getElementsByTagName(s)[0];
   if (d.getElementById(id)) {return;}
   js = d.createElement(s); js.id = id;
   js.src = "//connect.facebook.net/en_US/sdk.js";
   fjs.parentNode.insertBefore(js, fjs);
 }(document, 'script', 'facebook-jssdk'));
  </script>
   
  
  
  
  
  
  <div class=" main">
			<div class="show-top-grids">
				<div class="col-sm-10 show-grid-left main-grids" align="center">
					

					<div class="recommended">
						<div class="recommended-grids english-grid">
							
							<div class="recommended-info">
								<div class="heading">
									<h3 id="fb-welcome"></h3>
								</div>
								<div class="heading-right">
									<a  href="#small-dialog8" class="play-icon popup-with-zoom-anim">Play</a>
								</div>
								<div class="heading-right">
									<a  href="#small-dialog8" class="play-icon popup-with-zoom-anim">Summit</a>
								</div>
								<div class="clearfix"> </div>
							</div>

						</br>
						</br>
						</br>

							<div class="col-md-3 resent-grid recommended-grid movie-video-grid col-md-offset-4">
								<div class="resent-grid-img recommended-grid-img">
									<a href="#"><img src="images/blueblouse.jpg" alt="" /></a>
									<div class="time small-time show-time movie-time">
										<p>78 %</p>
									</div>
									<div class="clck movie-clock">
										<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
									</div>
								</div>
								<div class="resent-grid-info recommended-grid-info recommended-grid-movie-info">
									<h5><a href="single.html" class="title">Office wear # Any</a></h5>
									<ul>
										<li><p class="author author-info"><a href="#" class="author">Thara Perera</a></p></li>
										<li class="right-list"><p class="views views-info">2,114 played</p></li>
									</ul>
								</div>
							</div>
							
							
							
							<div class="clearfix"> </div>
						</div>
					</div>


					<!---->
					<div class="recommended">
						<div class="recommended-grids">
							
							<div class="col-md-3 resent-grid recommended-grid movie-video-grid col-md-offset-2" >
								<div class="resent-grid-img recommended-grid-img">
									<a href="#"><img src="images/blackdenim.jpg" alt="" /></a>
									<div class="time small-time show-time movie-time">
										<p>2:34</p>
									</div>
									<div class="clck movie-clock">
										<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
									</div>
								</div>
								<div class="resent-grid-info recommended-grid-info recommended-grid-movie-info">
									<h5><a href="single.html" class="title"></a></h5>
									<ul>
										<li><p class="author author-info"><a href="#" class="author"></a></p></li>
										<li class="right-list"><p class="views views-info"></p></li>
									</ul>
								</div>
							</div>
							<div class="col-md-3 resent-grid recommended-grid movie-video-grid col-md-offset-2">
								<div class="resent-grid-img recommended-grid-img">
									<a href="#"><img src="images/blueskirt.jpg" alt="" /></a>
									<div class="time small-time show-time movie-time">
										<p>3:45</p>
									</div>
									<div class="clck movie-clock">
										<span class="glyphicon glyphicon-time" aria-hidden="true"></span>
									</div>
								</div>
								<div class="resent-grid-info recommended-grid-info recommended-grid-movie-info">
									<h5><a href="single.html" class="title"></a></h5>
									<ul>
										<li><p class="author author-info"><a href="#" class="author"></a></p></li>
										<li class="right-list"><p class="views views-info"></p></li>
									</ul>
								</div>
							</div>
							
							
							<div class="clearfix"> </div>
						</div>
					</div>
					<!---->
					
					
				</div>
				<div class="col-md-2 show-grid-right">
					<h3>My Account</h3>
					<div class="show-right-grids">
						<ul>
							<li class="tv-img"><a href="#"><img src="images/mv.png" alt="" /></a></li>
							<li><a href="#">View Profile</a></li>
						</ul>
					</div>
					<div class="show-right-grids">
						<ul>
							<li class="tv-img"><a href="#"><img src="images/mv.png" alt="" /></a></li>
							<li><a href="#">Upload Item</a></li>
						</ul>
					</div>
					<div class="show-right-grids">
						<ul>
							<li class="tv-img"><a href="#"><img src="images/mv.png" alt="" /></a></li>
							<li><a href="#">Change Des</a></li>
						</ul>
					</div>
					<div class="show-right-grids">
						<ul>
							<li class="tv-img"><a href="#"><img src="images/mv.png" alt="" /></a></li>
							<li><a href="#">Change Pivot</a></li>
						</ul>
					</div>
					<div class="show-right-grids">
						<ul>
							<li class="tv-img"><a href="#"><img src="images/mv.png" alt="" /></a></li>
							<li><a href="#">SuitsU</a></li>
						</ul>
					</div>
					<div class="show-right-grids">
						<ul>
							<li class="tv-img"><a href="#"><img src="images/mv.png" alt="" /></a></li>
							<li><a href="#">Settings</a></li>
						</ul>
					</div>
					<div class="show-right-grids">
						<ul>
							<li class="tv-img"><a href="#"><img src="images/mv.png" alt="" /></a></li>
							<li><a href="#">Help</a></li>
						</ul>
					</div>
				</div>
				<div class="clearfix"> </div>
			</div>
  
  
  
  
  
  
    
</body>
</html>
